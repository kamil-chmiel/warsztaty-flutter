import 'package:flutter/material.dart';
import 'package:qrcode/qrcode.dart';
import 'package:url_launcher/url_launcher.dart';

import 'scan_button.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  QRCaptureController _captureController = QRCaptureController();
  bool _isScannerEnabled = false;
  String _scannedData;

  @override
  void initState() {
    super.initState();
    _captureController.onCapture((data) => _onBarcodeScanned(data));
  }

  onScannerButtonPressed() {
    _isScannerEnabled
        ? _captureController.pause()
        : _captureController.resume();
    setState(() {
      _isScannerEnabled = !_isScannerEnabled;
      _scannedData = null;
    });
  }

  _onBarcodeScanned(String data) {
    setState(() {
      _isScannerEnabled = false;
      _scannedData = data;
    });
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print('Can not launch url: $url');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            _isScannerEnabled
                ? Stack(children: <Widget>[
                    QRCaptureView(controller: _captureController),
                    Center(
                        child: SizedBox(
                            height: 180,
                            width: 180,
                            child:
                                Image.asset('assets/images/scanner_frame.png')))
                  ])
                : Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: _scannedData != null
                        ? Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                                Text('Barcode data:',
                                    textAlign: TextAlign.center,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                InkWell(
                                  onTap: () {
                                    _launchURL(_scannedData);
                                  },
                                  child: Text(_scannedData,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.blue,
                                          decoration:
                                              TextDecoration.underline)),
                                )
                              ])
                        : Text('Press \'Scan\' to start scanning')),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 90,
                  color: Colors.white,
                  child: Center(
                      child: ScanButton(
                    isScannerEnabled: _isScannerEnabled,
                    onButtonPressed: onScannerButtonPressed,
                  ))),
            )
          ],
        ),
      ),
    );
  }
}
