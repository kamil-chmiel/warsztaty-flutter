import 'package:flutter/material.dart';

class ScanButton extends StatelessWidget {
  final Function onButtonPressed;
  final bool isScannerEnabled;

  ScanButton({this.isScannerEnabled, this.onButtonPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      textColor: Colors.white,
      color: isScannerEnabled ? Colors.red : Colors.blue,
      child: Text(isScannerEnabled ? 'Stop scanning' : 'Scan'),
      onPressed: onButtonPressed,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
    );
  }
}
